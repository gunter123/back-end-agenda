package org.gunter.agenda.repository;

import org.gunter.agenda.domain.Evenement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EvenementRepository extends JpaRepository<Evenement, Long> {

}
