package org.gunter.agenda.api;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.gunter.agenda.domain.Agenda;
import org.gunter.agenda.request.AgendaRequest;
import org.gunter.agenda.request.response.Mute;
import org.gunter.agenda.service.AgendaService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;


@RestController
@RequestMapping("agendas")
@Slf4j
public class AgendaRestController {

    private final AgendaService agendaService;

    public AgendaRestController(AgendaService agendaService)
    {
        this.agendaService = agendaService;
    }

    @Operation(summary = "Create a Agenda", description = "Create a Agenda", tags = "Agenda")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Create an Agenda",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Agenda.class))}),
            @ApiResponse(responseCode = "500", description = "INTERNAL_SERVER_ERROR",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Error.class))})
    })
    @PostMapping(value = "")
    @ResponseBody private ResponseEntity<Agenda> saveAgenda(
            @Parameter(schema = @Schema(implementation = AgendaRequest.class), description = "The agenda request")
            @Valid @RequestBody AgendaRequest agendaRequest)
    {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/users/agendas").toUriString());
        return ResponseEntity.created(uri).body(agendaService.createAgenda(agendaRequest));
    }


    @Operation(summary = "Find all agendas", description = "Find all agendas", tags = "Agenda")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Find all agendas",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Agenda.class))}),
            @ApiResponse(responseCode = "500", description = "INTERNAL_SERVER_ERROR",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Error.class))})
    })
    @GetMapping("")
    private ResponseEntity<Mute.PageTransform> getAllAgendas(
            @Parameter(name = "page", description = "The page number of user set in dataset.", example = "2")
            @RequestParam(defaultValue = "0") int page,

            @Parameter(name = "size",  description = "The number of element to return per page.", example = "2")
            @RequestParam(defaultValue = "10") int size)
    {
        return ResponseEntity.ok().body(Mute.run(agendaService.getAllAgendas(page, size)));
    }


    @Operation(summary = "Find agenda by id.", description = "Find agenda by id.", tags = "Agenda")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Find agenda by id.",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Agenda.class))}),
            @ApiResponse(responseCode = "500", description = "INTERNAL_SERVER_ERROR",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Error.class))})
    })
    @GetMapping("/{id}")
    private ResponseEntity<Agenda> getAgendaById(
            @Parameter(name = "id", description = "The agenda's id", example = "202")
            @PathVariable(value = "id") Long id)
    {
        return ResponseEntity.ok().body(agendaService.findAgendaById(id));
    }


    @Operation(summary = "Delete agenda by agenda.", description = "Delete agenda by agenda.", tags = "Agenda")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Delete agenda by agenda."),
            @ApiResponse(responseCode = "500", description = "INTERNAL_SERVER_ERROR",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Error.class))})
    })
    @DeleteMapping("/{id}")
    private ResponseEntity<Void> deleteAgendaById(
            @Parameter(name = "id", description = "The agenda's id", example = "202")
            @PathVariable("id") Long id)
    {
        agendaService.deleteAgendaById(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }


    @Operation(summary = "Update agenda.", description = "Update agenda.", tags = "Agenda")
    @ApiResponses(value = {
            @ApiResponse(responseCode ="200" , description = "Update agenda.",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Agenda.class))}),
            @ApiResponse(responseCode = "500", description = "INTERNAL_SERVER_ERROR",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Error.class))})
    })
    @PutMapping("/{id}")
    private ResponseEntity<Agenda> updatedAgenda(
            @Parameter(name = "id", description = "The agenda's id", example = "202")
            @Valid @PathVariable("id") Long id,

            @Parameter(description = "The agenda request to updated")
            @Valid @RequestBody AgendaRequest agendaRequest)
    {
        return ResponseEntity.status(HttpStatus.OK).body(agendaService.updateAgenda(id, agendaRequest));
    }

}