package org.gunter.agenda.api;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.gunter.agenda.domain.Evenement;
import org.gunter.agenda.request.EvenementRequest;
import org.gunter.agenda.request.response.Mute;
import org.gunter.agenda.service.EvenementService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;


@RestController
@RequestMapping("evenements")
@Slf4j
public class EvenementRestController {

    private final EvenementService evenementService;

    public EvenementRestController(EvenementService evenementService)
    {
        this.evenementService = evenementService;
    }

    @Operation(summary = "Create a Evenement", description = "Create a Evenement", tags = "Evenement")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Create an Evenement",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Evenement.class))}),
            @ApiResponse(responseCode = "500", description = "INTERNAL_SERVER_ERROR",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Error.class))})
    })
    @PostMapping(value = "")
    @ResponseBody private ResponseEntity<Evenement> saveEvenement(
            @Parameter(schema = @Schema(implementation = EvenementRequest.class), description = "The evenement request")
            @Valid @RequestBody EvenementRequest evenementRequest)
    {
        URI uri = URI.create(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/users/evenements").toUriString());
        return ResponseEntity.created(uri).body(evenementService.createEvenement(evenementRequest));
    }


    @Operation(summary = "Find all evenements", description = "Find all evenements", tags = "Evenement")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Find all evenements",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Evenement.class))}),
            @ApiResponse(responseCode = "500", description = "INTERNAL_SERVER_ERROR",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Error.class))})
    })
    @GetMapping("")
    private ResponseEntity<Mute.PageTransform> getAllEvenements(
            @Parameter(name = "page", description = "The page number of user set in dataset.", example = "2")
            @RequestParam(defaultValue = "0") int page,

            @Parameter(name = "size",  description = "The number of element to return per page.", example = "2")
            @RequestParam(defaultValue = "10") int size)
    {
        return ResponseEntity.ok().body(Mute.run(evenementService.getAllEvenements(page, size)));
    }


    @Operation(summary = "Find evenement by id.", description = "Find evenement by id.", tags = "Evenement")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Find evenement by id.",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Evenement.class))}),
            @ApiResponse(responseCode = "500", description = "INTERNAL_SERVER_ERROR",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Error.class))})
    })
    @GetMapping("/{id}")
    private ResponseEntity<Evenement> getEvenementById(
            @Parameter(name = "id", description = "The evenement's id", example = "202")
            @PathVariable(value = "id") Long id)
    {
        return ResponseEntity.ok().body(evenementService.findEvenementById(id));
    }


    @Operation(summary = "Delete evenement by evenement.", description = "Delete evenement by evenement.", tags = "Evenement")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Delete evenement by evenement."),
            @ApiResponse(responseCode = "500", description = "INTERNAL_SERVER_ERROR",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Error.class))})
    })
    @DeleteMapping("/{id}")
    private ResponseEntity<Void> deleteEvenementById(
            @Parameter(name = "id", description = "The evenement's id", example = "202")
            @PathVariable("id") Long id)
    {
        evenementService.deleteEvenementById(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }


    @Operation(summary = "Update evenement.", description = "Update evenement.", tags = "Evenement")
    @ApiResponses(value = {
            @ApiResponse(responseCode ="200" , description = "Update evenement.",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Evenement.class))}),
            @ApiResponse(responseCode = "500", description = "INTERNAL_SERVER_ERROR",
                    content = {@Content(mediaType = "application/json", schema = @Schema(implementation = Error.class))})
    })
    @PutMapping("/{id}")
    private ResponseEntity<Evenement> updatedEvenement(
            @Parameter(name = "id", description = "The evenement's id", example = "202")
            @Valid @PathVariable("id") Long id,

            @Parameter(description = "The evenement request to updated")
            @Valid @RequestBody EvenementRequest evenementRequest)
    {
        return ResponseEntity.status(HttpStatus.OK).body(evenementService.updateEvenement(id, evenementRequest));
    }

}