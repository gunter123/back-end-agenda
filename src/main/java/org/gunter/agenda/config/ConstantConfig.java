package org.gunter.agenda.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component @Data
@ConfigurationProperties(prefix = "constantes")
public class ConstantConfig {
    String rve;
    String ise;
    String ftcr;
    String ftfr;
    String trrwnf;
    String rmns;
    String br;
}
