package org.gunter.agenda.config;

import io.swagger.v3.oas.models.*;
import io.swagger.v3.oas.models.headers.Header;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author Gunter
 *
 */
@Configuration
public class SwaggerAPIDocumentationConfig {

    private final String baseUrl= "test";

    @Bean
    public OpenAPI TestApi(){
        return new OpenAPI()
                .specVersion(SpecVersion.V30)
                .addSecurityItem(new SecurityRequirement())
                .externalDocs(externalDocumentation())
                .info(apiInfo())
                .paths(new Paths())
                .components(components());
    }

    private Info apiInfo()
    {
        return new Info()
                .version("V1")
                .title("test ")
                .description("Football (Private API) documentation")
                .contact(contact())
                .summary("String Summary")
                .termsOfService(" Terms of services link...")
                .license(license());
    }

    private ExternalDocumentation externalDocumentation()
    {
        return new ExternalDocumentation()
                .description("SpringShop Wiki Documentation")
                .url("https://springshop.wiki.github.org/docs");
    }

    private License license()
    {
        return new License()
                .name("Test Licence 1")
                .url("http://Test.com");
    }

//    private Server server() {
//        return new Server()
//                .url(this.baseUrl)
//                .description("gjgy"); //InetAddress.getLocalHost().getHostAddress()
//
//    }

    private Contact contact()
    {
        return new Contact()
                .name("test support technique")
                .url(this.baseUrl+"/supports")
                .email("paiecash@paiecash.com");
    }

    private Components components()
    {
        return new Components()
                .addHeaders("Accept", new Header().description("application/json"));
    }

}