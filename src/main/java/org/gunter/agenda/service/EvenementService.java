package org.gunter.agenda.service;


import org.gunter.agenda.domain.Evenement;
import org.gunter.agenda.request.EvenementRequest;
import org.springframework.data.domain.Page;

import java.util.List;

public interface EvenementService {

    Evenement createEvenement(EvenementRequest evenementRequest);

    void deleteEvenementById(Long id);

    Evenement findEvenementById(Long id);

    List<Evenement> getAllEvenement();

    Page<Evenement> getAllEvenements(int page, int size);

    Evenement updateEvenement(Long id, EvenementRequest evenementRequest);
}
