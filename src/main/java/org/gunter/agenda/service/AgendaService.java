package org.gunter.agenda.service;


import org.gunter.agenda.domain.Agenda;
import org.gunter.agenda.request.AgendaRequest;
import org.springframework.data.domain.Page;

import java.util.List;

public interface AgendaService {

    Agenda createAgenda(AgendaRequest agendaRequest);

    void deleteAgendaById(Long id);

    Agenda findAgendaById(Long id);

    List<Agenda> getAllAgenda();

    Page<Agenda> getAllAgendas(int page, int size);

    Agenda updateAgenda(Long id, AgendaRequest agendaRequest);
}
