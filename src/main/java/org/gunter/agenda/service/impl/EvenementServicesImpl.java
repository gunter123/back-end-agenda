package org.gunter.agenda.service.impl;


import lombok.extern.slf4j.Slf4j;
import org.gunter.agenda.domain.Agenda;
import org.gunter.agenda.domain.Evenement;
import org.gunter.agenda.repository.AgendaRepository;
import org.gunter.agenda.repository.EvenementRepository;
import org.gunter.agenda.request.EvenementRequest;
import org.gunter.agenda.service.EvenementService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@Slf4j
public class EvenementServicesImpl implements EvenementService {

    private EvenementRepository evenementRepository;
    private AgendaRepository agendaRepository;

    public EvenementServicesImpl(EvenementRepository evenementRepository, AgendaRepository agendaRepository)
    {
        this.evenementRepository = evenementRepository;
        this.agendaRepository = agendaRepository;
    }


    @Override
    public Evenement createEvenement(EvenementRequest evenementRequest)
    {
        log.info("Saving new evenement for agenda : {}  to the database", evenementRequest.getAgenda_id());
        Evenement evenement = new Evenement();
        evenement.setAgenda(agendaRepository.findById(evenementRequest.getAgenda_id()).get());
        return this.evenementRepository.save(evenement);
    }

    @Override
    public void deleteEvenementById(Long id)
    {
        log.info("deleting evenement by id {} to the database", id );
        this.evenementRepository.deleteById(id);
    }

    @Override
    public Evenement findEvenementById(Long id)
    {
        log.info("Fetching evenement by id {} to the database", id );
        return this.evenementRepository.findById(id).get();
    }


    @Override
    public List<Evenement> getAllEvenement()
    {
        log.info("Finding all evenements to the database");
        return this.evenementRepository.findAll();
    }

    @Override
    public Page<Evenement> getAllEvenements(int page, int size)
    {
        log.info("Fetching all roles to database.");
        Pageable pageable = PageRequest.of(page, size);
        return evenementRepository.findAll(pageable);
    }

    @Override
    public Evenement updateEvenement(Long id, EvenementRequest evenementRequest)
    {
        log.info("Updating agenda  : {}  to the database", id);
        Agenda agenda = agendaRepository.findById(evenementRequest.getAgenda_id()).get();
        Evenement evenement = evenementRepository.findById(id).get();

        evenement.setName(evenementRequest.getName());
        evenement.setDescription(evenementRequest.getDescription());
        evenement.setEnd_date(evenementRequest.getEnd_date());
        evenement.setStart_date(evenementRequest.getStart_date());
        evenement.setAgenda(agenda);
        return this.evenementRepository.save(evenement);
    }
}
