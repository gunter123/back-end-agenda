package org.gunter.agenda.service.impl;


import lombok.extern.slf4j.Slf4j;


import org.gunter.agenda.domain.Agenda;
import org.gunter.agenda.repository.AgendaRepository;
import org.gunter.agenda.request.AgendaRequest;
import org.gunter.agenda.service.AgendaService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class AgendaServicesImpl implements AgendaService {

    private AgendaRepository agendaRepository;

    public AgendaServicesImpl(AgendaRepository agendaRepository)
    {
        this.agendaRepository = agendaRepository;
    }


    @Override
    public Agenda createAgenda(AgendaRequest agendaRequest)
    {
        log.info("Saving new agenda for user : {}  to the database", agendaRequest.getName());
        Agenda agenda = new Agenda();
        agenda.setDescription(agendaRequest.getDescription());
        agenda.setName(agendaRequest.getName());
        return this.agendaRepository.save(agenda);
    }

    @Override
    public void deleteAgendaById(Long id)
    {
        log.info("deleting agenda by id {} to the database", id );
        this.agendaRepository.deleteById(id);
    }

    @Override
    public Agenda findAgendaById(Long id)
    {
        log.info("Fetching agenda by id {} to the database", id );
        return this.agendaRepository.findById(id).get();
    }


    @Override
    public List<Agenda> getAllAgenda()
    {
        log.info("Finding all agendas to the database");
        return this.agendaRepository.findAll();
    }

    @Override
    public Page<Agenda> getAllAgendas(int page, int size)
    {
        log.info("Fetching all roles to database.");
        Pageable pageable = PageRequest.of(page, size);
        return agendaRepository.findAll(pageable);
    }

    @Override
    public Agenda updateAgenda(Long id, AgendaRequest agendaRequest)
    {
        log.info("Updating agenda  : {}  to the database", id);
        Agenda agenda = agendaRepository.findById(id).get();

        agenda.setName(agendaRequest.getName());
        agenda.setDescription(agendaRequest.getDescription());
        return this.agendaRepository.save(agenda);
    }

}
