package org.gunter.agenda.domain;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
@EqualsAndHashCode
@Entity
@Table(name="Evenements")
public class Evenement {
    @Id
    @Column(updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    String name;

    String description;

    @ManyToOne
    @JoinColumn(name = "agenda_id")
    private Agenda agenda;

    @Temporal(value=TemporalType.TIMESTAMP)
    @Column(name="start_date", length = 30)
    Date start_date;

    @Temporal(value=TemporalType.TIMESTAMP)
    @Column(name="end_date", length = 30)
    Date end_date;


    @Temporal(value=TemporalType.TIMESTAMP)
    @Column(name="created_at", updatable = false, length = 30)
    private Date createdAt;

    @Temporal(value=TemporalType.TIMESTAMP)
    @Column(name="updated_at",nullable = false, length = 30)
    private Date updatedAt;

    @PrePersist
    protected void onCreate()
    {
        createdAt= new Date();
        updatedAt= new Date();
    }

    @PreUpdate
    protected void onUpdate()
    {
        updatedAt = new Date();
    }
}
