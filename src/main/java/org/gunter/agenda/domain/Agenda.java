package org.gunter.agenda.domain;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
@EqualsAndHashCode
@Entity
@Table(name="agendas")
public class Agenda {
    @Id
    @Column(updatable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String description;

    @Temporal(value=TemporalType.TIMESTAMP)
    @Column(name="created_at", nullable = false, updatable = false, length = 30)
    private Date createdAt;

    @Temporal(value=TemporalType.TIMESTAMP)
    @Column(name="updated_at",nullable = false, length = 30)
    private Date updatedAt;

    @PrePersist
    protected void onCreate()
    {
        createdAt= new Date();
        updatedAt= new Date();
    }

    @PreUpdate
    protected void onUpdate()
    {
        updatedAt = new Date();
    }
}
