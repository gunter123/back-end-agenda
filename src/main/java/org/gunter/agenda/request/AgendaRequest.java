package org.gunter.agenda.request;

import lombok.*;

@Data
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class AgendaRequest {
    String name;

    String description;
}
