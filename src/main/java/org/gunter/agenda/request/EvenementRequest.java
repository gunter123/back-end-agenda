package org.gunter.agenda.request;

import lombok.*;

import java.util.Date;

@Data
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class EvenementRequest {

    Long agenda_id;

    String name;

    String description;

    Date start_date;

    Date end_date;
}
