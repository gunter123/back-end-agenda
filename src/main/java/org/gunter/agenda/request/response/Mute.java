package org.gunter.agenda.request.response;

import lombok.Data;
import org.springframework.data.domain.Page;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class Mute {
    @Data
    public static class PageTransform {
        List<?> datas;
        Integer currentPage ;
        Integer size;
        Boolean sorted;
        Integer totalPages;
        Long totalItems;
    }

    private static PageTransform response;
    private static SimpleDateFormat simpleDateFormat;

    public Mute() {
        simpleDateFormat = new SimpleDateFormat("dd-MMMMMMM-yyyy HH:mm:ss", Locale.ENGLISH);
        response = new PageTransform();
    }


    /*  Transformer
    * ************************************************************************************************
    * ************************************************************************************************
     */
    public static PageTransform run(Page<?> page)
     {
         response = new PageTransform();
        List<?> datas = page.getContent();
        response.setDatas(datas);
        response.setCurrentPage(page.getNumber());
        response.setSize(page.getSize());
        response.setSorted(page.getSort().isUnsorted());
        response.setTotalPages(page.getTotalPages());
        response.setTotalItems(page.getTotalElements());
        return response;
    }




}
