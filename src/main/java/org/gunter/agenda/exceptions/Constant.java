package org.gunter.agenda.exceptions;

public class Constant {
    public static final String REQUEST_VALIDATION_ERRORS = "Request validation errors";
    public static final String INTERNAL_SERVER_ERROR = "Internal Server Error";
    public static final String FILED_TO_CREATE_RESOURCE = "Failed to create Resource.";
    public static final String FILED_TO_FIND_RESOURCE = "Failed to find Resource.";
    public static final String RESOURCE_NOT_FOUND_EXCEPTION = "The requested Resource was not found.";
    public static final String METHOD_NOT_ALLOWED_EXCEPTION = "Request method not supported.";
    public static final String BAD_REQUEST_EXCEPTION = "Bad request";
}
