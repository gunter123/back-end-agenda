package org.gunter.agenda.exceptions;

import lombok.Data;

@Data
public class Error {

    String type;
    int status;
    String error;
    String message;
    Object raison;
}
