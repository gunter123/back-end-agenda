package org.gunter.agenda.exceptions;

import org.gunter.agenda.config.ConstantConfig;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


@RestControllerAdvice
public class GlobalExceptionHandler {

    private Error error = new Error();
    private ConstantConfig constant = new ConstantConfig();

    // METHOD NO VALID EXCEPTION
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    @ExceptionHandler(value ={HttpRequestMethodNotSupportedException.class})
    public ResponseEntity<Error> handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException exception)
    {
        error.setType(constant.getRmns());
        error.setStatus(HttpStatus.METHOD_NOT_ALLOWED.value());
        error.setMessage(exception.getMessage());
        error.setError(constant.getIse());
        error.setRaison( exception.getSupportedMethods());

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(value ={MethodArgumentTypeMismatchException.class})
    public ResponseEntity<Error> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException exception)
    {
        error.setType("Failed to get value");
        error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        error.setMessage(exception.getMessage());
        error.setError(exception.getName());
        error.setRaison( exception.getMostSpecificCause().getMessage());

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(value ={MethodArgumentNotValidException.class})
    public ResponseEntity<?> handleValidationExceptions(MethodArgumentNotValidException ex)
    {

        error.setType(constant.getRve());
        error.setError(constant.getBr());
        error.setMessage("The required field is mandatory.");
        error.setStatus(HttpStatus.BAD_REQUEST.value());

        List<String> errorList = new ArrayList<>();
        for (ObjectError err : ex.getBindingResult().getAllErrors()) {
            if (err.getDefaultMessage()!=null){
                errorList.add(err.getDefaultMessage());
            }
        }
        error.setRaison(errorList);

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }

    // ERROR CREATING RESOURCE EXCEPTION
    @ExceptionHandler(value = {DataIntegrityViolationException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<?> handleCreationResourceExceptions(DataIntegrityViolationException exception)
    {
        error.setType(constant.getFtcr());
        error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        error.setMessage(exception.getCause().getMessage());
        error.setError(constant.getIse());
        error.setRaison(exception.getMostSpecificCause().getMessage());

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }

    // EMPTY DATA EXCEPTION
    @ExceptionHandler(value = {NoSuchElementException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<?> handleNoSuchElementException(NoSuchElementException exception)
    {
        error.setType(constant.getFtfr());
        error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        error.setMessage(exception.getMessage());
        error.setError(constant.getIse());
        error.setRaison(exception.getMessage());

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }

    // EMPTY DATA ACCESS EXCEPTION
    @ExceptionHandler(value = {EmptyResultDataAccessException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<?> handleEmptyResultDataAccessException(EmptyResultDataAccessException exception)
    {
        error.setType(constant.getTrrwnf());
        error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        error.setMessage(exception.getMessage());
        error.setError(constant.getIse());
        error.setRaison(exception.getMostSpecificCause().getMessage());

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }

    // INVALIDE DATA ACCESS API EXCEPTION
    @ExceptionHandler(value = {InvalidDataAccessApiUsageException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<?> handleEmptyResultDataAccessException(InvalidDataAccessApiUsageException exception)
    {
        error.setType(constant.getTrrwnf());
        error.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        error.setMessage(exception.getMessage());
        error.setError(constant.getIse());
        error.setRaison(exception.getMostSpecificCause().getMessage());

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }

}
