${AnsiBackground.BLACK}${AnsiColor.BRIGHT_GREEN}
     _       ____      _      _   _   ____       _         ____   _   _   _   _   _____   _____   ____
    / \     / ___|    / \    | \ | | |  _ \     / \       / ___| | | | | | \ | | |_   _| | ____| |  _ \
   / _ \   | |  _    / _ \   |  \| | | | | |   / _ \     | |  _  | | | | |  \| |   | |   |  _|   | |_) |
  / ___ \  | |_| |  / ___ \  | |\  | | |_| |  / ___ \    | |_| | | |_| | | |\  |   | |   | |___  |  _ <
 /_/   \_\  \____| /_/   \_\ |_| \_| |____/  /_/   \_\    \____|  \___/  |_| \_|   |_|   |_____| |_| \_\

${application.title} ${application.version}
Aganda Electronic (1.0.0)
Powered by Spring Boot ${spring-boot.version}